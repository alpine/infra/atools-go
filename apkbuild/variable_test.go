package apkbuild

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"mvdan.cc/sh/v3/syntax"
)

func makeAssigns(program string) *syntax.Assign {
	file := MakeFile(program)
	parsedFile, _ := syntax.NewParser().Parse(file, "APKBUILD")
	statement := parsedFile.Stmts[0]
	assigns := statement.Cmd.(*syntax.CallExpr).Assigns
	return assigns[0]
}

func TestVariableParamExpressions(t *testing.T) {
	type testTable struct {
		name         string
		program      string
		expectedVars []string
	}

	testcases := []testTable{
		{"EmptyString", `FOO=`, []string{}},
		{"EmptyStringDblQuote", `FOO=""`, []string{}},
		{"NakedVar", `FOO=$FOO`, []string{"FOO"}},
		{"NakedVarWithLiteral", `FOO=$FOO\ bar`, []string{"FOO"}},
		{"NakedVarWithSingleQuotedLiteral", `FOO=$FOO' bar'`, []string{"FOO"}},
		{"DblQuotedWithVar", `FOO="$FOO"`, []string{"FOO"}},
		{"DblQuotedWithBraces", `FOO="${FOO}"`, []string{"FOO"}},
		{"DblQuotedWithBracesAndLiteral", `FOO="${FOO} abc"`, []string{"FOO"}},
		{"DblQuotedWithMultiVarsAndLiteral", `FOO="${FOO} abc ${BAR}"`, []string{"FOO", "BAR"}},
		{"DblQuotedWithReplacement", `FOO="${FOO/a/} abc"`, []string{"FOO"}},
	}

	for _, testcase := range testcases {
		t.Run(testcase.name, func(t *testing.T) {
			assert := assert.New(t)
			require := require.New(t)

			assign := makeAssigns(testcase.program)
			variable := Variable{
				Assignment: assign,
			}
			expressions := variable.ParamExpressions()

			require.Len(expressions, len(testcase.expectedVars))
			for i, name := range testcase.expectedVars {
				assert.Equal(name, expressions[i].Param.Value)
			}
		})

	}
}
