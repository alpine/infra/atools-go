package main

import (
	"fmt"
	"os"

	"alpinelinux.org/go/atools/linter"
)

func main() {
	var filename string
	if len(os.Args) >= 2 {
		filename = os.Args[1]
	} else {
		filename = "APKBUILD"
	}

	if _, err := os.Stat(filename); os.IsNotExist(err) {
		fmt.Printf("Error: File %s does not exist\n", filename)
		os.Exit(2)
	}

	file, err := os.Open(filename)

	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(2)
	}

	violations := linter.Check(file, filename)

	for _, violation := range violations {
		fmt.Printf("%s\n", violation)
	}

	if len(violations) > 0 {
		os.Exit(1)
	}
}
