package main

import (
	"fmt"
	"log"
	"os"

	"mvdan.cc/sh/v3/syntax"
)

func main() {
	var file *os.File
	var filename string
	var err error

	if len(os.Args) >= 2 && os.Args[1] != "-" {
		filename = os.Args[1]

		file, err = os.Open(filename)

		if err != nil {
			fmt.Printf("%s\n", err)
			os.Exit(1)
		}
	} else {
		filename = "-"
		file = os.Stdin
	}

	parser := syntax.NewParser(syntax.Variant(syntax.LangBash))

	f, err := parser.Parse(file, filename)

	if err != nil {
		log.Fatal(err)
	}

	syntax.DebugPrint(os.Stdout, f)
	fmt.Println()
}
