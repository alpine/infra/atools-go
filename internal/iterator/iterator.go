package iterator

// Iterator is an interface that provides the methods to iterate over a
// collection.
//
// Use the HasNext method to verify there are still elements left, and the Next
// method to retrieve the next element.
type Iterator[E any] interface {
	HasNext() bool
	Next() E
	Cur() E
}

type SliceIterator[T ~[]E, E any] struct {
	elems []E
	index int
}

var _ Iterator[struct{}] = (*SliceIterator[[]struct{}, struct{}])(nil)

func NewSliceIterator[T ~[]E, E any](elems T) *SliceIterator[T, E] {
	return &SliceIterator[T, E]{
		elems: elems,
	}
}

func (i *SliceIterator[T, E]) HasNext() bool {
	return i.index < len(i.elems)
}

func (i *SliceIterator[T, E]) Next() (elem E) {
	if i.HasNext() {
		elem = i.elems[i.index]
		i.index++
	}
	return
}

func (i *SliceIterator[T, E]) Cur() (elem E) {
	if i.HasNext() {
		elem = i.elems[i.index]
	}

	return
}
