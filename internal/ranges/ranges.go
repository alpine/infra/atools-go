package ranges

type Range struct {
	Min int
	Max int
}

func (r Range) IsWithin(v int) bool {
	return v >= r.Min && v <= r.Max
}

type Ranges []Range

func (rs Ranges) IsWithinAny(v int) bool {
	for _, r := range rs {
		if r.IsWithin(v) {
			return true
		}
	}
	return false
}
