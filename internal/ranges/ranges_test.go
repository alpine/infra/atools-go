package ranges

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRangeWithin(t *testing.T) {
	type TestCase struct {
		Name     string
		Min      int
		Max      int
		Value    int
		IsWithin bool
	}

	testcases := []TestCase{
		{"BelowMinimum", 5, 10, 3, false},
		{"AtMinimum", 5, 10, 5, true},
		{"Within", 5, 10, 7, true},
		{"AtMaximum", 5, 10, 10, true},
		{"AboveMaximum", 5, 10, 15, false},
	}

	for _, testcase := range testcases {
		t.Run(testcase.Name, func(t *testing.T) {
			require := require.New(t)

			rnge := Range{testcase.Min, testcase.Max}
			beOrNot := ""
			if !testcase.IsWithin {
				beOrNot = "not "
			}
			require.Equal(
				testcase.IsWithin,
				rnge.IsWithin(testcase.Value),
				"%d should %sbe within range of %d and %d",
				testcase.Value,
				beOrNot,
				testcase.Min,
				testcase.Max,
			)
		})
	}
}

func TestRangesWithin(t *testing.T) {
	assert := assert.New(t)
	ranges := Ranges{
		{3, 8},
		{12, 20},
		{25, 30},
	}

	assert.True(ranges.IsWithinAny(5))
	assert.True(ranges.IsWithinAny(14))
	assert.False(ranges.IsWithinAny(1))
	assert.False(ranges.IsWithinAny(22))

}
