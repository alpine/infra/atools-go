pkgname=my-pkg
pkgver=1.2.3

cat >filename <<EOF
This is a test
    with spaces.
That should not trigger AL07
EOF

f() {
	cat >filename <<EOF
This should neither trigger AL07
   when indented with spaces
EOF
}
