pkgname=my-pkg
pkgver=1.2.3

export CFLAGS="${CFLAGS/-g/} -O2"

build() {
	make
}
