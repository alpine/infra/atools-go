pkgname=my-pkg
pkgver=1.2.3
source="
	https://github.com/example/project/archive/v1.2.3/project-v1.2.3-tar.gz
	fix-build-issue.patch::https://github.com/example/project/commit/abcdef1234.patch
	"
